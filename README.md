# Vault-Of-The-Deaths-Warrior


## Công nghệ sử dụng
```
Unity
Jetbrains Rider
Blender
Photoshop
Fork
```
## Kĩ thuật sử dụng
```
Particle System
Visual Effect
Shader Graph
PlayerPref For Unity
Cinemacine
AI Navigation
Timeline
Post Processing
Input System
```
## Design Pattern
```
Finite State Machine
Observer
OOP
Singleton
```

## AI
```
Finite State Machine
AI Navigation
```

## Cốt truyện game
```
------- VN
Ngày xửa ngày xưa, ở Eldoria vùng đất của kiếm và pháp thuật, ở đó có một ngôi làng nhỏ là Venoria, Venoria ẩn chứa một nguồn năng lượng được tương truyền rằng đã nuôi dưỡng cả Eldoria. có truyền thuyết rằng con người có thể đạt được sự bất tử nếu có thể kiểm soát được nó, một chiến binh huyền thoại mang tên Arakos đã tới đây vì sự ham muốn sức mạnh và khát khao sự bất tử, ông ta đã chiếm lấy nguồn sức mạnh này nhưng do không thể kiểm soát linh hồn ông ta đã bị bóng tối nuốt chửng và trở thành "Death Warrior". Mất kiểm soát, hắn dùng ma thuật hắc ám hút hết sự sống ở Eldoria và tạo ra sự kinh hoàng cho cả nơi đây


Cuối cùng, các pháp sư vĩ đại đã hợp lực và phong ấn Arakos trong một hầm ngục sâu dưới lòng đất, được gọi là Vault of the Death Warrior. Trước khi bị phong ấn, Arakos đã nguyền rủa Eldoria rằng nếu hầm ngục bị mở ra, bóng tối sẽ tràn ngập thế giới và Arakos sẽ trở lại để hoàn thành nhiệm vụ hủy diệt của mình.

Người chơi sẽ vào vai Vera hoặc Harry, một thiếu niên trẻ từ làng Venoria. họ tình cờ phát hiện ra mình mang trong mình sức mạnh của các vị thần. Cùng với phát hiện này, họ quyết định lên đường ngăn chặn sự hồi sinh của Arakos.

Một ngày của ngàn năm sau cánh cửa của Vault of the Death Warrior đã mở ra báo hiệu sự trở lại của Arakos.

Là người mang trong mình sứ mạng tiêu diệt Arakos để mang lại hoà bình cho Eldoria, bạn đã trải qua rèn luyện gian khổ và vượt qua chặng đường dầy chông gai, giờ đây bạn đang đứng trước cánh cửa để bước vào trong hầm ngục phong ấn Arakos.

------- EN
Once upon a time, in Eldoria, the land of swords and magic, there was a small village called Venoria. Venoria contained a source of energy that was said to have nourished all of Eldoria. There is a legend that humans can achieve immortality if they can control it, a legendary warrior named Arakos came here because of his desire for power and desire for immortality, he took over. This source of power, but because he could not control his soul, he was swallowed by darkness and became a "Death Warrior". Losing control, he used dark magic to drain all life in Eldoria and create terror throughout the place.

Finally, the great magicians joined forces and sealed Arakos in a deep underground dungeon, known as the Vault of the Death Warrior. Before being sealed, Arakos cursed Eldoria that if the dungeon was opened, darkness would flood the world and Arakos would return to complete his mission of destruction.

The player takes on the role of Vera or Harry, a young teenager from the village of Venoria. They accidentally discovered that they had the power of the gods. With this discovery, they decided to set out to prevent the revival of Arakos.

One day a thousand years later the doors of the Vault of the Death Warrior opened, heralding the return of Arakos.

As someone with a mission to destroy Arakos to bring peace to Eldoria, you have undergone arduous training and overcome a difficult path, now you are standing in front of the door to enter the windy dungeon. Arakos seal.
```
## Nội dung game và thể loại
```
- Trang bị vũ khí vật phẩm và kĩ năng sau đó khám phá dungeon đánh bại quái vật và qua các màn chơi
- Thể loại: Hack N Slash, Isometric 3D
```
## Giải quyết bài toán
```
1. Tính chân thực và hấp dẫn: Đồ họa 3D tạo ra một thế giới sống động và chân thực hơn, giúp người chơi đắm chìm trong trò chơi và trải nghiệm mọi thứ một cách sinh động hơn.
2. Khả năng tương tác: Game 3D cho phép người chơi tương tác với môi trường và các nhân vật trong game theo nhiều cách phức tạp hơn, tăng tính chân thực và thú vị.
3. Giải trí và giảm căng thẳng: Chơi game 3D là một hình thức giải trí hiệu quả, giúp giảm căng thẳng và mang lại niềm vui cho người chơi.
4. Tính đa dạng và linh hoạt: Game 3D có thể đáp ứng nhu cầu giải trí của nhiều đối tượng người chơi khác nhau, từ trẻ em đến người lớn, từ người yêu thích phiêu lưu đến người thích chiến lược. Sự đa dạng này giúp game 3D trở thành một hình thức giải trí phổ biến và hấp dẫn.
5. Sáng tạo và tưởng tượng: Game 3D thường khuyến khích người chơi sử dụng trí tưởng tượng và sáng tạo để giải quyết các tình huống và thử thách trong game. Điều này giúp người chơi phát triển khả năng sáng tạo và tưởng tượng của mình.
6. Thúc đẩy ngành công nghiệp game và công nghệ: Sự phát triển của game 3D đã thúc đẩy sự tiến bộ trong các lĩnh vực công nghệ liên quan như đồ họa máy tính, trí tuệ nhân tạo, và phần cứng. Điều này không chỉ làm phong phú ngành công nghiệp game mà còn có tác động tích cực đến nhiều ngành công nghiệp khác.
7. Khuyến khích sự kiên nhẫn và kiên trì: Game 3D thường đòi hỏi người chơi phải kiên nhẫn và kiên trì để hoàn thành các nhiệm vụ khó khăn, từ đó rèn luyện tính kiên trì và sự quyết tâm.
```